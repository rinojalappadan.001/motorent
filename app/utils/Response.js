
export default class Response {
    constructor() {
      this.statusCode = null;
      this.type = null;
      this.data = null;
      this.message = null;
    }
    setSuccess(statusCode,message,data) {
        this.statusCode = statusCode;
        this.data = data;
        this.message = message;
        this.type = 'success';
    }
    setError(statusCode,message){
        this.statusCode = statusCode;
        this.message = message;
        this.type = 'error';
    }
    send(res){
        const result = {
            status: this.type,
            message: this.message,
            data:this.data
        };
        // if (global.postedVal > 0) {
        //     const postData = {
        //       respData: result,
        //       respStatus: this.statusCode,
        //     };
        //     logService.update(postData);
        //   }
        if(this.type === 'success'){
            return res.status(this.statusCode).json(result);
        }
        else{
            return res.status(this.statusCode).json({
                status: this.type,
                message: this.message,
            });
        }
    }
}



/////

