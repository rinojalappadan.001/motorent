
const db=[
    {title:'Sonata hero',path:'https://www.extremetech.com/wp-content/uploads/2019/12/SONATA-hero-option1-764A5360-edit-640x354.jpg'},
    {title:'ZS EV',path:'https://static.india.com/wp-content/uploads/2020/12/MG-ZS-EV.jpg'},
    {title:'Mahindra XUV700',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/42355/xuv700-exterior-right-front-three-quarter.jpeg?isig=0&q=85&wm=1'},
    {title:'Tata Tigor EV',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/45849/tigor-ev-facelift-exterior-right-front-three-quarter-5.jpeg?isig=0&q=85&wm=1'},
    {title:'Hyundai Venue',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/35455/venue-exterior-right-front-three-quarter-2.jpeg?q=85&wm=1'},
    {title:'Maruti Suzuki Swift',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/54399/exterior-right-front-three-quarter-10.jpeg?q=85&wm=1'},
    {title:'Hyundai Creta',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/41564/hyundai-creta-right-front-three-quarter9.jpeg?q=85&wm=1'},
    {title:'Tata Altroz',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/32597/tata-altroz-right-front-three-quarter20.jpeg?q=85&wm=1'},
    {title:'Tata Nexon',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/41645/tata-nexon-right-front-three-quarter3.jpeg?q=85&wm=1'},
    {title:'Maruti Suzuki Baleno',path:'https://imgd.aeplcdn.com/1056x594/cw/ec/37710/Maruti-Suzuki-Baleno-Right-Front-Three-Quarter-147420.jpg?wm=1&q=85'},
    {title:'Mahindra Thar',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/40087/thar-exterior-right-front-three-quarter-11.jpeg?q=85&wm=1'},
    {title:'Honda Amaze',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/45951/amaze-facelift-exterior-right-front-three-quarter.jpeg?isig=0&q=85&wm=1'},
    {title:'Hyundai i20',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg?q=85&wm=1'},
    {title:'Maruti Suzuki Ertiga ',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/35211/ertiga-exterior-right-front-three-quarter-141879.jpeg?q=85&wm=1'},
    {title:'Maruti Suzuki Vitara Brezza',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/39028/marutisuzuki-vitara-brezza-right-front-three-quarter3.jpeg?q=85&wm=1'},
    {title:'Kia Sonet Right Front Three Quarter',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/41523/sonet-exterior-right-front-three-quarter-110.jpeg?q=85&wm=1'},
    {title:'Kia Seltos',path:'https://imgd.aeplcdn.com/1056x594/n/cw/ec/33372/seltos-exterior-right-front-three-quarter-3.jpeg?q=85&wm=1'}
]

exports.getAllImages = (req,res)=>{
    return res.status(200).json(db);
}