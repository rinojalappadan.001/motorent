const express = require('express');
const imagesController = require ('../controllers/images.controller');
module.exports = (app)=>{
    const router = express.Router();
    router.get('/',imagesController.getAllImages);
    app.use('/api/images',router);
}