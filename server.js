const express = require('express');
const bodyParser = require('body-parser');
require('dotenv')();

const app = express();
const PORT = process.env.PORT||8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true}));

require('./app/routes/images.routes')(app);

app.listen(PORT,(err)=>{
 if(err)throw err;
 console.log(`Server listening on port ${PORT}`);
});